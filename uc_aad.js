$Id:$

/**
 * Gemini Ajax Attributes
 */

(function($) {

Drupal.UbercartAAD = {};

Drupal.UbercartAAD.initialize = function() {
  var nid = Drupal.settings.UbercartAAD.nid;
  var ajax_attribute_update = 0;
  uc_aad_initialize_fields(nid);
  uc_aad_perform_ajax_update(nid);
};

/**
 * Initialize select attributes so that attributes update on change, radio
 * buttons update on click.
 */


function uc_aad_initialize_fields(nid) {
  $("#node-" + nid + " select").each(function () {
    var element = this;
    if (element.name && element.name.match("attributes")) {
      
      $(this).change(function () {
        uc_aad_perform_ajax_update(nid);
      });
    }
  });
  $("#node-'. $nid .' :radio").each(function () {
    var element = this;
    if (element.name && element.name.match("attributes")) {
      $(this).click(function () {
        uc_aad_perform_ajax_update(nid);
      });
    }
  });
}

function uc_aad_perform_ajax_update(nid) {
  var ajax_attribute_form = document.createElement("div");
  ajax_attribute_form.setAttribute( "target", "" );
  ajax_attribute_form.setAttribute( "action", Drupal.settings.UbercartAAD.callback_url );
  ajax_attribute_form.setAttribute( "method", "post" );
  ajax_attribute_form.elements = [];
  var element = document.getElementById(nid+"_uc_aad_nid");
  ajax_attribute_form.elements[0] = element;
  $("#node-" + nid + " select").each(function () {
    var element = this;
    if (element.name && element.name.match("attributes")) {
      ajax_attribute_form.elements[ajax_attribute_form.elements.length] = element;
    }
  });
  $("#node-" + nid + " :radio").each(function () {
    var element = this;
    if (element.name && element.name.match("attributes")) {
      ajax_attribute_form.elements[ajax_attribute_form.elements.length] = element;
    }
  });
  var this_update = new Date();
  ajax_attribute_update = this_update.getTime();
  $(ajax_attribute_form).ajaxSubmit( {
    dataType: "json",
    success: function(json_results) {
      if (ajax_attribute_update == this_update.getTime()) {
        for (var i in json_results.dependencies) {
          var atr = json_results.dependencies[i];
          var hide = false;
          // if there is no selection, the option should be hidden
          if (!json_results.attributes[atr.dependency]) {
            hide = true;
          }
          else {
            // hide the selector if it's dependency has a void option selected
            for (var void_option in atr.void_options) {
              if (json_results.attributes[atr.dependency] == atr.void_options[void_option]) {
                hide = true;
              }
            }
          }
          if (hide) {
            $("#edit-attributes-" + i + "-wrapper").hide();
          } else {
            $("#edit-attributes-" + i + "-wrapper").show();
          }
        }
      }
    },
    error: function() {
   //   alert("Attribute update failed.");
    }
  });
}

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    Drupal.UbercartAAD.initialize();
  });
}

})(jQuery);
